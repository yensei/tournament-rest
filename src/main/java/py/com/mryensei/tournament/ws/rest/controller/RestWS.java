package py.com.mryensei.tournament.ws.rest.controller;

abstract class RestWS {
	
	public static final String DUMMY_USER = "/rest/usuario/dummy";
    public static final String GET_USER = "/rest/usuario/{id}";
    public static final String GET_ALL_USER = "/rest/usuarios";
    public static final String CREATE_USER = "/rest/usuario/create";
    public static final String DELETE_USER = "/rest/usuario/delete/{id}";
}
