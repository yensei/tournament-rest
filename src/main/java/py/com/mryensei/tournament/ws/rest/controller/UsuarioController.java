package py.com.mryensei.tournament.ws.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import py.com.mryensei.tournament.ws.rest.model.Usuario;
import py.com.yensei.tournament.dao.jsonobj.DaoUsuario;

@Controller
public class UsuarioController {
	
	private static final Logger logger = Logger.getLogger(UsuarioController.class);
	
	private Map<Integer, Usuario> userData = new HashMap<Integer, Usuario>();
	
	@RequestMapping(value = RestWS.DUMMY_USER , method = RequestMethod.GET)
    public @ResponseBody Usuario getDummyUser() {
        logger.info("Start getDummyUser");
        Usuario user = new Usuario();
        user.setId(9999);
        user.setNombre("Dummy");
        user.setFechaNacimiento(new Date());
        userData.put(9999, user);
        return user;
    }
	
	
	@RequestMapping(value = RestWS.GET_USER, method = RequestMethod.GET)
    public @ResponseBody Usuario getUser(@PathVariable("id") int userId) {
        logger.info("Start getUser. ID="+userId);
        return userData.get(userId);
    }
	
	@RequestMapping(value = RestWS.CREATE_USER, method = RequestMethod.POST)
	public void createUser(@RequestBody Usuario user ) {
		logger.info("Start request "+RestWS.CREATE_USER);
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			DaoUsuario.createUser(mapper.writeValueAsString(user));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = RestWS.GET_ALL_USER,method = RequestMethod.GET)
	public @ResponseBody List<Usuario> getAllUser(
			@RequestParam(required=false, value="begin") int begin,	
			@RequestParam(required=false, value="end" ) int end){
		logger.info("Start request "+RestWS.GET_ALL_USER);
//		return new ArrayList<Usuario>();
		ObjectMapper mapper = new ObjectMapper();
		
		List<Usuario> list = new ArrayList<Usuario>();
		
		try {
			mapper.readValue(DaoUsuario.getAll(), Usuario[].class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return list;
	}
	
}
